import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, Picker, TextInput, Switch } from 'react-native';
import createDrawerNavigator, { SafeAreaView } from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import ModalSelector from 'react-native-modal-selector';
import moment from 'moment';

import Products from '../components/Products'
import { electronics } from '../Data'
import { connect } from 'react-redux'
import ShoppingCartIcon from '../screens/shopping-cart-icon'

const mocks = [
  {
    id: 1,
    time: '6:00 AM',
  },
  {
    id: 2,
    time: '7:00 AM',
  },
  {
    id: 3,
    time: '8:00 AM',
  },
  {
    id: 4,
    time: '9:00 AM',
  },
  {
    id: 5,
    time: '10:00 AM',
  },
  {
    id: 6,
    time: '11:00 AM',
  },
  {
    id: 7,
    time: '12:00 PM',
  },
  {
    id: 8,
    time: '1:00 PM',
  },
  {
    id: 9,
    time: '2:00 PM',
  },
  {
    id: 10,
    time: '3:00 PM',
  },
  {
    id: 11,
    time: '4:00 PM',
  },
  {
    id: 12,
    time: '5:00 PM',
  },
  {
    id: 13,
    time: '6:00 PM',
  },
  {
    id: 14,
    time: '7:00 PM',
  },
  {
    id: 15,
    time: '8:00 PM',
  },
  {
    id: 16,
    time: '9:00 PM',
  },
  {
    id: 17,
    time: '10:00 PM',
  },
  {
    id: 18,
    time: '11:00 PM',
  },
  {
    id: 19,
    time: '12:00 AM',
  },
]

class Booking extends Component {
  constructor(props) {
    super(props);

    this.state = {
        textInputValue: '',
        title: this.props.navigation.state.params.title,
        price: this.props.navigation.state.params.price,
        rates: this.props.navigation.state.params.rates,
        image: this.props.navigation.state.params.image,
        datePick: '',
        timePick: '',
        itemPressed: '',
        rate: '',
        opening: false,
        text: '',
        plan: '',
    },
    this.onDayPress = this.onDayPress.bind(this);
  }

  onDayPress(day) {
    this.setState({ 
      selected: day.dateString,
      datePick: day.dateString,
    })
    var test = day.dateString;
   
  }

  pickTime(item) {
      this.setState({ 
        timePick: item.time, 
        itemPressed: item.id,
      })

  }

  openCart() {
    if (!this.state.opening) {
      this.props.navigation.push('Cart')
        this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  addTodo = () => {
    var dateSummary = moment(this.state.datePick).format('MM/DD');
    this.props.dispatch({type: 'ADD_TODO', title: this.state.title, price: this.state.rate, date: dateSummary, image: this.state.image, time: this.state.timePick, plan: this.state.plan})
    this.setState({ rate: '', datePick: '', timePick: '', })
  }


  renderRecommendedTime = (item, index) => {
    return (
      <View style={{flexDirection: 'column', flexWrap: 'wrap',}}>
        <FlatList
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'hidden'}}
          numColumns={3}
          data={this.props.destinations}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderRecommendationTime(item, index)}
        />
      </View>
    )
  }

  renderRecommendationTime = (item, index) => {

    return (
      <View style={{ width: 33.333333333333333 + '%', height: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingRight: 5,}}>
        <TouchableOpacity onPress={this.pickTime.bind(this, item)}>
          <View style={[styles.listEventsTime, {backgroundColor: this.state.itemPressed  === item.id ? 'black' : 'white',} ]}>
            <View style={{width: 100 + '%',  justifyContent:'center', flexDirection: 'row', alignItems: 'center'}}>
              <Text style={[styles.summaryTimeSlot, {color: this.state.itemPressed  === item.id ? 'white' : 'black',}]}>{item.time}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  render() {

    var dateSummary = moment(this.state.datePick).format('LL');
    var dateDays = moment(this.state.datePick).format('ddd');

    return (
      <View style={{flex: 1}}>
       <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.header}>
                <View style={{width: 40 + '%', height: 50, justifyContent: 'center', }}>
                  <TouchableOpacity onPress={() => this.props.navigation.goBack()}> 
                    <Image
                      style={styles.back}
                      source={require('../assets/img/left-arrow-final.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 60 + '%', height: 50,justifyContent: 'center', alignItems: 'flex-end',}}>
                  <Text style={styles.ratesHeading}>{this.state.title}</Text>
                </View>
              </View>
              <Calendar
                // Specify style for calendar container element. Default = {}
                style={{
                  marginRight: 25,
                  marginLeft: 25,
                  paddingLeft: 0,
                  paddingRight: 0,
                  height: 370,
                }}
                onDayPress={this.onDayPress}
                markedDates={{[this.state.selected]: {selected: true, marked: true}}}
                // markedDates={{
                //   '2019-06-17': {marked: true, dotColor: 'black'},
                // }}
                // Specify theme properties to override specific styles for calendar parts. Default = {}
                theme={{
                  'stylesheet.calendar.header': {
                    monthText: {
                      //backgroundColor: 'blue',
                      fontSize: 25,
                      fontFamily: 'HelveticaNeueLTStd-Bd',
                      color: '#000000',
                      paddingTop: 13, 
                    },
                    week: {
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                      marginTop: 25,
                    },
                    todayText: {
                      color: 'pink',
                    },
                  },
                  backgroundColor: '#ffffff',
                  calendarBackground: '#ffffff',
                  textSectionTitleColor: 'red',
                  selectedDayBackgroundColor: '#000000',
                  selectedDayTextColor: '#ffffff',
                  todayTextColor: 'red',
                  dayTextColor: '#2d4150',
                  textDisabledColor: '#d9e1e8',
                  dotColor: '#00adf5',
                  selectedDotColor: '#ffffff',
                  arrowColor: 'black',
                  monthTextColor: 'black',
                  indicatorColor: 'blue',
                  textDayFontFamily: 'HelveticaNeueLTStd-Bd',
                  textMonthFontFamily: 'HelveticaNeueLTStd-Bd',
                  textDayHeaderFontFamily: 'HelveticaNeueLTStd-Bd',
                  textDayFontWeight: '700',
                  textMonthFontWeight: 'bold',
                  textDayHeaderFontWeight: '300',
                  textDayFontSize: 16,
                  textMonthFontSize: 18,
                  textDayHeaderFontSize: 14,
                }}
              />
         
          <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.welcome}>Summary</Text>
            </View>
            {this.renderRecommendedTime()}
            <TouchableOpacity style={styles.plan}>
                <ModalSelector
                    data={this.state.rates}
                    initValue="Select Plan!"
                    // onChange={(option)=>{ alert(`${option.label} (${option.key}) nom nom nom`) }} />
                    onChange={(option)=>{ this.setState({ 
                      rate: option.rate,
                      plan: option.label,
                    }) }} /> 
            </TouchableOpacity>
              <View style={styles.listEvents}>
                <View style={{width: 30 + '%', height: 50, backgroundColor: 'white', justifyContent:'center', flexDirection: 'row', alignItems: 'flex-start'}}>
                  <Text style={styles.summaryTime}>{this.state.timePick}</Text>
                </View>
                <View style={{width: 70 + '%', height: 50, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center'}}>
                  <Text style={styles.summaryDate}>{dateDays}, {dateSummary}</Text>
                  <Text style={styles.summaryCategory}>&#8369; {this.state.rate}</Text>
                </View>
              </View>
            {/* <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15,}}>
              <TouchableOpacity onPress={this.openCart.bind(this)} style={styles.book}>
                <Text style={styles.textBook}>ADD TO CART</Text>
              </TouchableOpacity>
            </View> */}
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, marginBottom: 100,}}>
            <TouchableOpacity onPress={() => this.addTodo()} style={styles.book}>
              <Text style={styles.textBook}>ADD TO CART</Text>
            </TouchableOpacity>
            </View>
          </View>
        
          </ScrollView>
            <ShoppingCartIcon />     
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addItemToCart: (product) => dispatch({ type: 'ADD_TO_CART', payload: product })
  }
}

export default connect()(Booking);

Booking.defaultProps = {
  destinations: mocks,
};

const styles = StyleSheet.create({
  plan: { 
    //backgroundColor: 'red', 
    borderTopWidth: 1, 
    borderTopColor: 'black',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    borderLeftColor: 'black',
    borderLeftWidth: 1,
    borderRightColor: 'black',
    borderRightWidth: 1,
    marginBottom: 25,
    marginTop: 15,
  },
  textBook: {
    textAlign: 'center',
    fontSize: 24,
    color: 'white',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
  },
  book: {
    marginBottom: 40,
    backgroundColor: 'black',
    width: 100 + '%',
    borderRadius: 20,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    ...Platform.select({
      ios: {             
        paddingTop: 15,
      },
      android: {
        paddingTop: 10,
        paddingBottom: 10,
      },
    }),
  },
  summaryTime: {
    color: '#000000',
    paddingTop: 2,
  },  
  summaryTimeSlot: {
    color: '#000000',
  },  
  summaryCategory: {
    color: 'gray',
    fontSize: 16,
  },
  summaryDate: {
    color: '#000000',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
    fontSize: 18,
  },
  listEvents: {
      borderRadius: 16,
      paddingHorizontal: 10,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.10,
          shadowRadius: 16,
          elevation: 24,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  listEventsTime: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row', 
    borderRadius: 16,
    backgroundColor: 'white',
    width: 100 + '%',
    height: 35,
    ...Platform.select({
            ios: {             
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 6,
              },
              shadowOpacity: 0.10,
              shadowRadius: 8,
              elevation: 16,
            },
            android: {
              shadowOpacity: 0.75,
              shadowRadius: 5,
              shadowColor: 'black',
              shadowOffset: { height: 0, width: 0 },
              elevation: 10,
              borderRadius: 16,
            },
    }),
  },
//   listEventsTime: {
//     height: 35,
//     borderRadius: 16,
//     paddingHorizontal: 5,
//     justifyContent: 'center',
//     alignItems: 'center',
//     flexDirection: 'row', 
//     width: 100 + '%', 
//     backgroundColor: 'white',
//     ...Platform.select({
//       ios: {             
//         shadowColor: "#000",
//         shadowOffset: {
//           width: 0,
//           height: 6,
//         },
//         shadowOpacity: 0.10,
//         shadowRadius: 16,
//         elevation: 24,
//       },
//       android: {
//         shadowOpacity: 0.75,
//         shadowRadius: 5,
//         shadowColor: 'black',
//         shadowOffset: { height: 0, width: 0 },
//         elevation: 10,
//         borderRadius: 16,
//       },
//   }),
// },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //backgroundColor: 'red'
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesHeading: {
    fontSize: 20,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitleList: {
    paddingTop: 20,
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitle: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  back: {
    width: 16,
    height: 16,
  },
  header: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    //justifyContent: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
