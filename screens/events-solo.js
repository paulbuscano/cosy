import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, Picker } from 'react-native';
import createDrawerNavigator, { SafeAreaView } from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import moment from 'moment';

export default class EventsSolo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      title: this.props.navigation.state.params.title,
      description: this.props.navigation.state.params.description,
      date: this.props.navigation.state.params.date,
      id: this.props.navigation.state.params.id,
    };        
  }

  render() {
    var wow = moment(this.state.date).format('LL');
    var eventTime = moment(this.state.date).format('LT');

    var strDescription = this.state.description;
    var resDescription = strDescription.split('=');

    var strPrice = resDescription[1];
    var resPrice = strPrice.split(':');

    var str = this.state.date;
    var res = str.split('-');

    return (
      <ImageBackground 
        source={{ uri: 'https://cosy.ph/wp-content/uploads/'+ res[0] +'/'+ res[1] +'/'+ this.state.id +'.jpg'}}
        imageStyle={{ opacity: 0.8, }}
        style={{resizeMode: 'cover',flex: 1, backgroundColor: '#000000',}}
      >
      <View style={{flex: 1}}>

       <ScrollView showsVerticalScrollIndicator={false}>

                <View style={styles.header}>
                    <View style={{width: 50 + '%', height: 50, justifyContent: 'center', }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}> 
                            <Image
                            style={styles.back}
                            source={require('../assets/img/left-arrow-white.png')}
                            />
                        </TouchableOpacity>
                    </View>
                <View style={{width: 50 + '%',  height: 50,justifyContent: 'center', alignItems: 'flex-end',}}>
                  <Text style={styles.ratesHeading}>&#8369;{resPrice[1]}/pax</Text>
                </View>
                </View>
                <View style={{flex: 1, flexDirection: 'column', paddingHorizontal: 25, paddingTop: 25,}}>
                    <View style={{width: 100 + '%',  paddingTop: 200,}}>
                        <Text style={styles.titleText}>{this.state.title}</Text>
                    </View>
                    <View style={{width: 100 + '%', marginBottom: 10,}}>
                        <Text style={{color: 'white'}}>Time: {eventTime}</Text>
                    </View>
                    <View style={{width: 100 + '%', marginBottom: 20,}}>
                        <Text style={{color: '#e3e3e3'}}>{resDescription[0]}</Text>
                    </View> 
                </View>
                <View style={{flex: 1, flexDirection: 'row', paddingHorizontal: 25, paddingTop: 20,}}>
                  <View style={{width: 35 + '%', height: 50, }}>
                    <View style={{flex: 1, flexDirection: 'row',justifyContent: 'center', alignItems: 'center',}}>
                        <TouchableOpacity style={styles.book}>
                          <Text style={styles.textBook}>BUY</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{width: 65 + '%', height: 50,  flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                    <Text style={styles.eventDate}>{wow}</Text>
                  </View>
                </View>
          </ScrollView>
      </View>
      </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  eventDate:{ 
    fontSize: 18,
    color: '#fff',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
    ...Platform.select({
      ios: {             
          paddingTop: 10,
      },
      android: {
       
      },
  }),
  },
  buyContainer: {
    borderRadius: 25,
    
  },
  buyText: {
    borderRadius: 25,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 10,
    paddingTop: 10,
    backgroundColor: 'black',
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
  },
    hours: {
        color : '#000',
        paddingTop: 15,
        fontWeight: '700',
    },
    aboutArea: {
        color : '#000',
    },
    amenitiesText: {
        color: '#000',
    },
    fromText: {
        fontSize: 18,
        color: '#fff',
        fontWeight: '700',
    },
    subTitle: {
        color: '#e1e1e1',
        fontWeight: '700',
        fontSize: 13,
    },
  titleText: {
    color: '#ffffff',
    fontSize: 40,   
    fontFamily: 'HelveticaNeueLTStd-Bd', 
    fontWeight: '700',
  },
  plan: { 
    //backgroundColor: 'red', 
    borderTopWidth: 1, 
    borderTopColor: 'black',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    borderLeftColor: 'black',
    borderLeftWidth: 1,
    borderRightColor: 'black',
    borderRightWidth: 1,
    marginBottom: 25,
  },
  textBook: {
    textAlign: 'center',
    fontSize: 18,
    color: 'black',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
  },
  book: {
    backgroundColor: 'white',
    width: 100 + '%',
    borderRadius: 20,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    ...Platform.select({
      ios: {             
        paddingTop: 10,
      },
      android: {
        paddingTop: 10,
        paddingBottom: 10,
      },
    }),
  },
  summaryTime: {
    color: '#000000',
    paddingTop: 2,
  },  
  summaryCategory: {
    color: 'gray',
    fontSize: 16,
  },
  summaryDate: {
    color: '#000000',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
    fontSize: 18,
  },
  listEvents: {
      borderRadius: 16,
      paddingHorizontal: 10,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.10,
          shadowRadius: 16,
          elevation: 24,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    ...Platform.select({
        ios: {             
          paddingTop: 10,
        },
        android: {
         
        },
      }),
    //backgroundColor: 'red'
  },
  titleAmenities: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 20,
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesHeading: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitleList: {
    paddingTop: 20,
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitle: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  back: {
    width: 16,
    height: 16,
  },
  header: {
    //backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    //justifyContent: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    //paddingTop: 30,
    borderTopRightRadius: 18, 
    borderTopLeftRadius: 18,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
