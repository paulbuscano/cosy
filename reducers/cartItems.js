let nextId = 0;
const cartItems = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TO_CART':
            return [...state, action.payload]
        case 'ADD_TODO':
            return [...state, {
                id: nextId++,
                title: action.title,
                plan: action.plan,
                price: action.price,
                date: action.date,
                time: action.time,
                image: action.image,
            }]
        case 'REMOVE_FROM_CART':
            return state.filter(cartItem => cartItem.id !== action.payload.id)
    }

    return state
}

export default cartItems