import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card, SocialIcon } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

export default class About extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
              <View style={styles.header}>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                      style={styles.menu}
                      source={require('../assets/img/menu.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.logo}
                    source={require('../assets/img/logo.png')}
                  />
                </View>
              </View>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
              <View style={{flex: 1, flexDirection: 'row', marginBottom: 20,}}>
                  <ImageBackground 
                    source={{ uri: 'https://scontent.fmnl4-2.fna.fbcdn.net/v/t1.0-9/50217191_1211521252334384_7404274986942201856_o.jpg?_nc_cat=101&_nc_oc=AQmxWKyJrYWTBhnDko6z94DR97KL-5ECFJFCDX-uShkB91lUXoHPZQPGAuWcp4MT2Vc&_nc_ht=scontent.fmnl4-2.fna&oh=d6dfb60e45168932731ae1d55a4e583b&oe=5D959016' }} 
                    imageStyle={{ borderRadius: 16, opacity: 0.8 }}
                    style={{width: 100 + '%', height: 200, backgroundColor: '#000000', borderTopRightRadius: 16, borderTopLeftRadius: 16,borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16}}
                  >
                  <View style={{ padding: 10, width: 100 + '%', top: 120, position: 'absolute',justifyContent: 'space-evenly',flexDirection: 'column',}}>
                    <Text style={styles.aboutTitle}>About Us</Text>
                  </View>
                  </ImageBackground>
            </View>
            <Text>A coworking space equipped with the neccessities to get the job done.</Text>
            <Text style={styles.aboutArea}>Situated at one of the busiest avenues of Makati, CoSY is a coworking space where excellence and convinience meets.</Text>
            <View style={styles.title}>
              <Text style={styles.welcome}>Reviews</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginBottom: 10,borderBottomColor: 'black', borderBottomWidth: 1, paddingBottom: 10,}}>
                <View style={{width: 25 + '%', justifyContent: 'center', alignItems: 'flex-start'}}>
                    <Image
                        style={{width: 64, height: 64,   borderRadius: 40,}}
                        source={{ uri: 'https://images.unsplash.com/photo-1456327102063-fb5054efe647?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'}}
                    />
                </View>
                <View style={{width: 75 + '%',  flexDirection: 'column'}}>
                    <Text style={styles.reviewName}>Jutt Sulit</Text>
                    <View style={{flexDirection: 'row',paddingBottom: 7,}}>
                        <Icon
                          name='ios-star'
                          color='#000000' />
                        <Icon
                          name='ios-star'
                          color='#000000' />
                        <Icon
                          name='ios-star'
                          color='#000000' />
                        <Icon
                          name='ios-star'
                          color='#000000' />
                         <Icon
                          name='ios-star-outline'
                          color='#000000' />
                    </View>
                    <Text style={styles.reviewText}>“No fuss. No frills. Love how "zen" and straightforward the place is.”</Text>
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginBottom: 10,borderBottomColor: 'black', borderBottomWidth: 1, paddingBottom: 10,}}>
                <View style={{width: 25 + '%',  justifyContent: 'center', alignItems: 'flex-start'}}>
                    <Image
                        style={{width: 64, height: 64,   borderRadius: 40,}}
                        source={{ uri: 'https://images.unsplash.com/photo-1441622915984-05d13dfb3d8c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'}}
                    />
                </View>
                <View style={{width: 75 + '%', flexDirection: 'column'}}>
                    <Text style={styles.reviewName}>Carlo Pamintuan</Text>
                    <View style={{flexDirection: 'row',paddingBottom: 7,}}>
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                    </View>
                    <Text style={styles.reviewText}>“Highly accessible. Perfect CBD office without the crazy parking fees!”</Text>
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', marginBottom: 10, borderBottomColor: 'black', borderBottomWidth: 1, paddingBottom: 10,}}>
                <View style={{width: 25 + '%', justifyContent: 'center', alignItems: 'flex-start'}}>
                    <Image
                        style={{width: 64, height: 64,   borderRadius: 40,}}
                        source={{ uri: 'https://images.unsplash.com/photo-1482268015045-1e9932ce47f7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80'}}
                    />
                </View>
                <View style={{width: 75 + '%',  flexDirection: 'column'}}>
                    <Text style={styles.reviewName}>Chuck Araneta</Text>
                    <View style={{flexDirection: 'row', paddingBottom: 7,}}>
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star'
                        color='#000000' />
                        <Icon
                        name='ios-star-outline'
                        color='#000000' />
                    </View>
                    <Text style={styles.reviewText}>“It is definitely more than a cozy place. I get to work while doing my laundry at the same time. How convenient is that?”</Text>
                </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center',}}>
              <View style={{width: 25 + '%',}}>
                <TouchableOpacity>
                  <SocialIcon
                    type='facebook'
                    iconColor='#000000'
                    style={{borderRadius: 0, backgroundColor: 'transparent', borderWidth: 0,}}
                    raised={false}
                    iconSize={30}
                  />
                </TouchableOpacity>
              </View>
              <View style={{width: 25 + '%',}}>
                <TouchableOpacity>
                  <SocialIcon
                    type='twitter'
                    iconColor='#000000'
                    style={{borderRadius: 0, backgroundColor: 'transparent', borderWidth: 0,}}
                    raised={false}
                    iconSize={30}
                  />
                </TouchableOpacity>
              </View>
              <View style={{width: 25 + '%',}}>
                <TouchableOpacity>
                  <SocialIcon
                    type='instagram'
                    iconColor='#000000'
                    style={{borderRadius: 0, backgroundColor: 'transparent', borderWidth: 0,}}
                    raised={false}
                    iconSize={30}
                  />
                </TouchableOpacity>
              </View>
              <View style={{width: 25 + '%',}}>
                <TouchableOpacity>
                  <SocialIcon
                    type='youtube'
                    iconColor='#000000'
                    style={{borderRadius: 0, backgroundColor: 'transparent', borderWidth: 0,}}
                    raised={false}
                    iconSize={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50,}}>
                <Text>Privacy & Terms</Text>
            </View>
          </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    reviewText: {
      color: '#555555',
    },
    reviewName: {
      fontWeight: '700',
      color: '#000000',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      paddingBottom: 3,
    },
    aboutArea: {
        color: '#000',
        marginBottom: 20,
    },
    listEvents: {
      paddingHorizontal: 10,
      justifyContent: 'flex-start',
      flexDirection: 'column',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,
          elevation: 12,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //backgroundColor: 'red'
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesTitleList: {
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  aboutTitle: {
    fontSize: 25,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  menu: {
    width: 24,
    height: 24,
  },
  header: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
