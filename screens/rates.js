import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

const mocks = [
  {
    id: 1,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Monthly',
    title: 'Virtual Office',
    description: 'Run your business from anywhere and build your presence in the market with a professional address in the heart of Makati.',
    price: '4,480',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/Desk-4.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Mail Handling',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/mail.png',
      },
      {
        id: 2,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
    ],
  },
  {
    id: 2,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Monthly',
    title: 'Private Office',
    description: 'Enclosed office with desks and access to amenities and complimentary drinks for your team — start small and add more seats as you grow.',
    price: '22,176',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/IMG_8528.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Print, Scan & Copy',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/photocopy-.png',
      },
      {
        id: 2,
        amenity: 'Local Calls',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-phone.png',
      },
      {
        id: 3,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 4,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
      {
        id: 5,
        amenity: '24/7 Access',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/security.png',
      },
      {
        id: 6,
        amenity: 'Perks & Discounts',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-perks.png',
      },
    ],
  },
  {
    id: 3,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Daily/10-Day/20-Day Pass/Monthly',
    title: 'Hotdesk',
    description: 'Get to choose your own seat and meet new friends in our plug-and-play ready and open work space.',
    price: '300',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-3.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Print, Scan & Copy',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/photocopy-.png',
      },
      {
        id: 2,
        amenity: 'Local Calls',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-phone.png',
      },
      {
        id: 3,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 4,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
    ],
  },
  {
    id: 4,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Hourly/4-Hour Booking',
    title: 'Event Hall',
    description: 'greece description',
    price: '4,000',
    review: 3212,
    image: 'https://scontent.fmnl4-4.fna.fbcdn.net/v/t1.0-9/50095566_1211524225667420_961735729824661504_o.jpg?_nc_cat=104&_nc_oc=AQmSyzFtibM7aUpwzP4UAj4J9DUEWgt9gwX5PpC1uXa3dPBDrL7k_0mJZcpLRIM8NG0&_nc_ht=scontent.fmnl4-4.fna&oh=bf68f84e3d170ecc7e2cc80dccaa5074&oe=5D8E3C16',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 2,
        amenity: '24/7 Security',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/security.png',
      },
    ],
  },
  {
    id: 5,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Hourly',
    title: 'Conference Room',
    description: 'Huddle your team for meetings or quick catch-up to spacious meeting rooms, equipped with sound system and complimentary refreshments for your team.',
    price: '800',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/IMG_8849-copy.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 2,
        amenity: 'SMART TV with HDMI',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/projector.png',
      },
    ],
  }
]

export default class Rates extends Component {

  constructor(props) {
    super(props);
    this.state = {
      opening: false,
    }
  }

  openRatesSolo(item) {
    if (!this.state.opening) {
      this.props.navigation.push('RatesSolo', {title: item.title, price: item.price, plan: item.plan, image: item.image, description: item.description, amenities: item.amenities, rates: item.rates,})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderRecommended = (item, index) => {
    return (
      <View>
        <FlatList
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'visible'}}
          data={this.props.destinations}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderRecommendation(item, index)}
        />
      </View>
    )
  }

  renderRecommendation = (item, index) => {
    return (
      <TouchableOpacity onPress={this.openRatesSolo.bind(this, item)}>
        <View style={{flex: 1, flexDirection: 'row', marginBottom: 20,}}>
            <ImageBackground 
              source={{ uri: item.image }}
              imageStyle={{ borderRadius: 16, opacity: 0.8 }}
              style={{width: 100 + '%', height: 200, backgroundColor: '#000000', borderTopRightRadius: 16, borderTopLeftRadius: 16,borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16}}
            >
            <View style={{ padding: 10, width: 100 + '%', top: 110, position: 'absolute',justifyContent: 'space-evenly',flexDirection: 'column',}}>
              <Text style={styles.ratesTitle}>{item.title}</Text>
              <Text style={styles.ratesPrice}>Starts at &#8369;{item.price}</Text>
            </View>
            </ImageBackground>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={{flex: 1}}>
            <View style={styles.header}>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                      style={styles.menu}
                      source={require('../assets/img/menu.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.logo}
                    source={require('../assets/img/logo.png')}
                  />
                </View>
            </View>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {this.renderRecommended()}
          </View>
          </ScrollView>
      </View>
    );
  }
}

Rates.defaultProps = {
  destinations: mocks,
};

const styles = StyleSheet.create({
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesTitle: {
    fontSize: 23,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  menu: {
    width: 24,
    height: 24,
  },
  header: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
