import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, StatusBar } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';

const mocks = [
  {
    id: 1,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Monthly',
    title: 'Virtual Office',
    description: 'Run your business from anywhere and build your presence in the market with a professional address in the heart of Makati.',
    price: '4,480',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/Desk-4.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 100,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 200,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 300,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Mail Handling',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/mail.png',
      },
      {
        id: 2,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
    ],
  },
  {
    id: 2,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Monthly',
    title: 'Private Office',
    description: 'Enclosed office with desks and access to amenities and complimentary drinks for your team — start small and add more seats as you grow.',
    price: '22,176',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/IMG_8528.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Print, Scan & Copy',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/photocopy-.png',
      },
      {
        id: 2,
        amenity: 'Local Calls',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-phone.png',
      },
      {
        id: 3,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 4,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
      {
        id: 5,
        amenity: '24/7 Access',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/security.png',
      },
      {
        id: 6,
        amenity: 'Perks & Discounts',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-perks.png',
      },
    ],
  },
  {
    id: 3,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Daily/10-Day/20-Day Pass/Monthly',
    title: 'Hotdesk',
    description: 'Get to choose your own seat and meet new friends in our plug-and-play ready and open work space.',
    price: '300',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-3.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Print, Scan & Copy',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/photocopy-.png',
      },
      {
        id: 2,
        amenity: 'Local Calls',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-phone.png',
      },
      {
        id: 3,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 4,
        amenity: 'Meeting Room',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/conference.png',
      },
    ],
  },
  {
    id: 4,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Hourly/4-Hour Booking',
    title: 'Event Hall',
    description: 'greece description',
    price: '4,000',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-2.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 2,
        amenity: '24/7 Security',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/security.png',
      },
    ],
  },
  {
    id: 5,
    user: {
      name: 'paul',
      avatar: '',
    },
    location: 'wallis',
    plan: 'Hourly',
    title: 'Conference Room',
    description: 'Huddle your team for meetings or quick catch-up to spacious meeting rooms, equipped with sound system and complimentary refreshments for your team.',
    price: '800',
    review: 3212,
    image: 'https://cosy.ph/wp-content/uploads/2018/12/IMG_8849-copy.jpg',
    rates: [
      {
        key: 0,
        label: 'Daily',
        rate: 300,
      },
      {
        key: 1,
        label: 'Weekly',
        rate: 500,
      },
      {
        key: 2,
        label: 'Monthly',
        rate: 4480,
      },
    ],
    amenities: [
      {
        id: 1,
        amenity: 'Refreshments',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/icon-coffee.png',
      },
      {
        id: 2,
        amenity: 'SMART TV with HDMI',
        amenityUrl: 'https://cosy.ph/wp-content/uploads/2018/09/projector.png',
      },
    ],
  }
]

export default class Home extends Component {
  static navigationOptions = {
    headerMode: 'none'
  }

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      dataSource: [],
      opening: false,
    }
  }

  componentDidMount() {
    this.getData();
  }
  

  getData = async () => {

    var request_1_url = 'https://www.googleapis.com/calendar/v3/calendars/2j77cq5h9a0veetd7c4nh8ltbc%40group.calendar.google.com/events?key=AIzaSyDmDR8qCaaqRK7bZ_pTBCUEk1zl7N35br8';
   
   return fetch(request_1_url).then((response) => response.json()).then((responseJson)  => {

        this.setState({
            isLoading: false,
            dataSource: responseJson.items,
        });
    });
  }

  openRatesSolo(item) {
    if (!this.state.opening) {
      this.props.navigation.push('RatesSolo', {title: item.title, price: item.price, plan: item.plan, image: item.image, description: item.description, amenities: item.amenities, rates: item.rates,})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  openEventsSolo(item) {
    if (!this.state.opening) {
      this.props.navigation.push('EventsSolo', {title: item.summary, description: item.description, date: item.start.dateTime, id: item.id})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  
  renderRecommended = (item, index) => {
    return (
      <View style={{ paddingBottom: 20 }}>
        <FlatList
          horizontal
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'visible'}}
          data={this.props.destinations}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderRecommendation(item, index)}
        />
      </View>
    )
  }

  renderRecommendation = (item, index) => {
    if(index === 0) {
      return (
        <TouchableOpacity onPress={this.openRatesSolo.bind(this, item)}>
          <View style={{ flexDirection: 'column', width: 130, margin: 10, marginLeft: 0 }}>
            <ImageBackground 
              style={{ backgroundColor: '#000000', borderTopRightRadius: 12, borderTopLeftRadius: 12, height: 200, borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}
              imageStyle={{ borderRadius: 16 , opacity: 0.8}}
              source={{ uri: item.image }}
            >
              <View>
                <View style={{ padding: 10,  width: 100 + '%', top: 120, position: 'absolute', justifyContent: 'space-evenly',flexDirection: 'column'}}>
                  <Text style={styles.recommend}>{item.title}</Text>
                </View>
              </View>
            </ImageBackground>
          </View>
        </TouchableOpacity>
      )
    }

    return (
      <TouchableOpacity onPress={this.openRatesSolo.bind(this, item)}>
        <View style={{ flexDirection: 'column', width: 130, margin: 10, }}>       
          <ImageBackground
            style={{ backgroundColor: '#000000',  borderTopRightRadius: 12, borderTopLeftRadius: 12, height: 200, borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16 }}
            imageStyle={{ borderRadius: 16, opacity: 0.8 }}
            source={{ uri: item.image }}
          >
            <View>
              <View style={{ padding: 10, width: 100 + '%', top: 120, position: 'absolute',justifyContent: 'space-evenly',flexDirection: 'column'}}>
                <Text style={styles.recommend}>{item.title}</Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    )
  }

  //events
  renderEvented = (item, index) => {
    console.log(this.state.dataSource, 'dataSource');
    return (
      <View style={{flex: 1, flexDirection: 'column', paddingBottom: 20,}}>
        <FlatList
          //horizontal
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'visible'}}
          data={this.state.dataSource}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderEvents(item, index)}
        />
      </View>
    )
  }


  renderEvents = (item, index) => {
 
    var str = item.start.dateTime;
    var res = str.split('-');

    var strDescription = item.description;
    var resDescription = strDescription.split('=');

    var exe = String.prototype.trim.call(resDescription[0]);

    var dateRight = moment(item.start.dateTime).format('ll');
    var eventTime = moment(item.start.dateTime).format('LT');

    return (
      <View>
        <TouchableOpacity onPress={this.openEventsSolo.bind(this, item)}  style={styles.shadow}>
          <View style={styles.boxEvents}>
              <Image
                style={styles.events}
              source={{ uri: 'https://cosy.ph/wp-content/uploads/'+ res[0] +'/'+ res[1] +'/'+ item.id +'.jpg'}}
            />
          </View>
        </TouchableOpacity>
        <Text style={styles.eventsTitle}>{item.summary}</Text>
        {/* <Text style={styles.excerpt}>{exe}</Text> */}
        <Text style={styles.hours}>Date: {dateRight}</Text>
        <Text style={styles.hoursTime}>Time: {eventTime}</Text>
      </View>
    )
  }


  render() {
    return (
      <View style={{flex: 1}}>
       <StatusBar barStyle="dark-content" />
          <View style={styles.header}>
              <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                  <Image
                    style={styles.menu}
                    source={require('../assets/img/menu.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                <Image
                  style={styles.logo}
                  source={require('../assets/img/logo.png')}
                />
              </View>
          </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {this.renderRecommended()}
            <View style={styles.title}>
              <Text style={styles.welcome}>Events</Text>
            </View>
            {this.renderEvented()}    
          </View>
        </ScrollView>

      </View>      
    );
  }
}   

Home.defaultProps = {
  destinations: mocks,
};

const styles = StyleSheet.create({
  cartTitle: {
    fontSize: 18,
    fontFamily: 'HelveticaNeueLTStd-Bd',
    textAlign: 'left',
    color: '#fff',
  },
  fixedView : {
    position: 'absolute',
    left: 0,
    bottom: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 100 + '%',
  },
  hoursTime: {
    fontWeight: '300',
    marginBottom: 30,
  },
  hours: {
    fontWeight: '300',
    marginBottom: 5,
  },
  excerpt: {
    color: '#000000',
    marginBottom: 5,
  },
  boxEvents: {
    flexDirection: 'row', 
    width: 100 + '%',
    ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,
          elevation: 12,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  events: {
    width: 100 + '%', 
    height: 200, 
    borderRadius: 16,
    shadowColor: "#000",
    backgroundColor: 'black',
  },
  recommend: {
    fontSize: 18,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //backgroundColor: 'red'
  },
  menu: {
    width: 24,
    height: 24,
  },
  cart: {
    width: 24,
    height: 24,
  },
  logo: {
    width: 76,
    height: 36,
  },
  header: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 26,
    //paddingBottom: 26,
    paddingHorizontal: 25,
    //backgroundColor: 'red'
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  instructions: {
    textAlign: 'center',
    color: '#fff',
    marginBottom: 20,
    backgroundColor: '#000',
    width: 35 + '%',
    paddingTop: 10,
    paddingBottom: 10,
    //paddingHorizontal: 5,
    borderRadius: 26,
    fontWeight: '700',
    marginTop: 10,
  },
  eventsTitle: {
    fontSize: 18,
    fontFamily: 'HelveticaNeueLTStd-Bd',
    textAlign: 'left',
    color: '#000000',
    marginBottom: 3,
    paddingTop: 15,
  },
});
