import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity} from 'react-native';
import createDrawerNavigator from 'react-navigation';
import {NavigationActions} from 'react-navigation';

export default class Sidebar extends Component {

  render() {
    if(this.props.activeItemKey === 'Home') {
        var imgSourceHome = require('../assets/img/home-active.png');
    } else {
        var imgSourceHome = require('../assets/img/home-not.png');
    }

    if(this.props.activeItemKey === 'Rates') {
        var imgSourceRates = require('../assets/img/survey-active.png');
    } else {
        var imgSourceRates = require('../assets/img/survey.png');
    }

    if(this.props.activeItemKey === 'Events') {
        var imgSourceEvents = require('../assets/img/calendar-active.png');
    } else {
        var imgSourceEvents = require('../assets/img/calendar.png');
    }

    if(this.props.activeItemKey === 'Gallery') {
        var imgSourceGallery = require('../assets/img/gallery-active.png');
    } else {
        var imgSourceGallery = require('../assets/img/gallery.png');
    }

    if(this.props.activeItemKey === 'About') {
        var imgSourceAbout = require('../assets/img/help-active.png');
    } else {
        var imgSourceAbout = require('../assets/img/help.png');
    }

    if(this.props.activeItemKey === 'Contact') {
        var imgSourceContact = require('../assets/img/contact-active.png');
    } else {
        var imgSourceContact = require('../assets/img/contact.png');
    }

    if(this.props.activeItemKey === 'Booking') {
        var imgSourceContact = require('../assets/img/contact-active.png');
    } else {
        var imgSourceContact = require('../assets/img/contact.png');
    }

    if(this.props.activeItemKey === 'RatesSolo') {
        var imgSourceContact = require('../assets/img/contact-active.png');
    } else {
        var imgSourceContact = require('../assets/img/contact.png');
    }

    if(this.props.activeItemKey === 'EventsSolo') {
        var imgSourceContact = require('../assets/img/contact-active.png');
    } else {
        var imgSourceContact = require('../assets/img/contact.png');
    }

    return (
      <View style={styles.container}>
            <View>
                <View style={{flexDirection: 'row', marginBottom: 30}}>
                    <TouchableOpacity onPress={() => this.props.navigation.closeDrawer()}> 
                        <View style={{width: 100 + '%', flexDirection: 'row', paddingHorizontal: 25, marginBottom: 20}}>
                            <View style={styles.imgCancel}>
                                <Image
                                    style={styles.close}
                                    source={require('../assets/img/cancel.png')}
                                />
                            </View>
                            
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection: 'row', width: 100 + '%'}}>
                    <View style={{width: 100 + '%', height: 10, borderBottomColor: 'black', borderBottomWidth: 1}} />
                </View>
            
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='Home' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceHome}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='Home' ? styles.activeColor : styles.notActiveColor )]}>HOME</Text>
                    </View>
                </View>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.props.navigation.navigate('Rates')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='Rates' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceRates}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='Rates' ? styles.activeColor : styles.notActiveColor )]}>RATES</Text>
                    </View>
                </View>
            </TouchableOpacity>

           <TouchableOpacity onPress={() => this.props.navigation.navigate('Events')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='Events' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceEvents}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='Events' ? styles.activeColor : styles.notActiveColor )]}>EVENTS</Text>
                    </View>
                </View>
            </TouchableOpacity>

           <TouchableOpacity onPress={() => this.props.navigation.navigate('Gallery')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='Gallery' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceGallery}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='Gallery' ? styles.activeColor : styles.notActiveColor )]}>GALLERY</Text>
                    </View>
                </View>
            </TouchableOpacity>

           <TouchableOpacity onPress={() => this.props.navigation.navigate('About')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='About' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceAbout}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='About' ? styles.activeColor : styles.notActiveColor )]}>ABOUT</Text>
                    </View>
                </View>
            </TouchableOpacity>

           <TouchableOpacity onPress={() => this.props.navigation.navigate('Contact')}> 
                <View style={[{flexDirection: 'row', width: 100 + '%', height: 50},(this.props.activeItemKey=='Contact' ? styles.activeBackgroundColor : null)]}>
                    <View style={{ width: 20 + '%' , justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Image
                            style={styles.menuIcon}
                            source={imgSourceContact}
                        />
                    </View>
                    
                    <View style={{ width: 80 + '%' , justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text  style={[styles.welcomeActive, (this.props.activeItemKey=='Contact' ? styles.activeColor : styles.notActiveColor )]}>CONTACT</Text>
                    </View>
                </View>
            </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <View style={{width: 100 + '%', height: 50, backgroundColor: 'white', justifyContent: 'flex-start',  paddingHorizontal: 30,}}>
                    <Text style={styles.privacy}>&#169; Copyright 2019</Text>
                </View>
            </View>
          
      </View>
    );
  }
}

const styles = StyleSheet.create({
    notActiveColor: {
        color: '#000',
    },
    activeColor: {
        color: '#fff',
    },
    activeBackgroundColor: {
        backgroundColor: 'black'
    },
    imgCancel: {
        width: 100 + '%',
        height: 50, 
        paddingTop: 50, 
       // paddingTop: 26, 
        justifyContent: 'center',
    },
    privacy: {
        textAlign: 'left',
        color: '#333333',
    },
    menuIcon: {
        width: 24,
        height: 24,
    },
    close: {
        width: 32,
        height: 32,
    },
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 100 + '%',
        flexDirection: 'column',
    },
    welcomeActive: {
        fontFamily: 'HelveticaNeueLTStd-Bd',
        fontSize: 20,
        ...Platform.select({
            ios: {     
                paddingTop: 10,
            },
            android: {
            //   height: 80,
            //   paddingTop: 20
            },
        }),
        paddingLeft: 20,
    },
    welcome: {
        fontFamily: 'HelveticaNeueLTStd-Bd',
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
