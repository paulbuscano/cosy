import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, } from 'react-native';

import Home from './screens/home';
import Rates from './screens/rates';
import Events from './screens/events';
import Gallery from './screens/gallery';
import About from './screens/about';
import Contact from './screens/contact';
import Booking from './screens/booking';
import RatesSolo from './screens/rates-solo';
import EventsSolo from './screens/events-solo';
import Cart from './screens/cart';
import Sidebar from './screens/sidebar';

import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { Provider } from 'react-redux'
import store from './store'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
     </Provider>
    )
  }
}

export default App;

const AppNavigator = createStackNavigator({
  Home: {
    screen: Home,
  },
  RatesSolo: {
    screen: RatesSolo,
  },
  EventsSolo: {
    screen: EventsSolo,
  },
},{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
 });
 
const RatesNavigator = createStackNavigator({
  Rates: {
    screen: Rates,
  },
  RatesSolo: {
    screen: RatesSolo,
  },
  Booking: {
    screen: Booking,
  },
  Cart: {
    screen: Cart,
  },
},{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const EventsNavigator = createStackNavigator({
  Events: {
    screen: Events,
  },
  EventsSolo: {
    screen: EventsSolo,
  },
},{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});


const MyDrawerNavigator = createDrawerNavigator({
  Home: {
    screen: AppNavigator,
  },
  Rates: {
    screen: RatesNavigator,
  },
  Events: {
    screen: EventsNavigator,
  },
  Gallery: {
    screen: Gallery,
  },
  About: {
    screen: About,
  },
  Contact: {
    screen: Contact,
  },
}, {
  contentComponent: Sidebar,
})

const AppContainer = createAppContainer(MyDrawerNavigator);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
