import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, Picker } from 'react-native';
import createDrawerNavigator, { SafeAreaView } from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

export default class RatesSolo extends Component {

  constructor(props) {
    super(props)
    this.state = {
      title: this.props.navigation.state.params.title,
      price: this.props.navigation.state.params.price,
      plan: this.props.navigation.state.params.plan,
      image: this.props.navigation.state.params.image,
      description: this.props.navigation.state.params.description,
      amenities: this.props.navigation.state.params.amenities,
      rates: this.props.navigation.state.params.rates,
      opening: false,
    };        
  }

  openBooking() {
    if (!this.state.opening) {
      this.props.navigation.push('Booking', {title: this.state.title, price: this.state.price, rates: this.state.rates, image: this.state.image})
        this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  renderFacilities = (item, index) => {
    return (
      <View>
        <FlatList
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'visible'}}
          data={this.state.amenities}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderAmenities(item, index)}
        />
      </View>
    )
  }

  renderAmenities = (item, index) => {
    return (
      <View style={{flex: 1, flexDirection: 'row', paddingBottom: 10,}}>
        <View style={{width: 25 + '%', height: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
            <Image
                style={{width: 32, height: 32,}}
                source={{uri: item.amenityUrl }}
            />
        </View>
        <View style={{width: 75 + '%', height: 50, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
            <Text style={styles.amenitiesText}>{item.amenity}</Text>
        </View>
      </View>
    )
  }

  render() {

    return (
      <View style={{flex: 1}}>

       <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground 
              source={{uri: this.state.image}}
              imageStyle={{ opacity: 0.8, }}
              style={{resizeMode: 'cover',flex: 1, backgroundColor: '#000000',}}
          >
                <View style={styles.header}>
                    <View style={{width: 50 + '%', height: 50, justifyContent: 'center', }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}> 
                            <Image
                            style={styles.back}
                            source={require('../assets/img/left-arrow-white.png')}
                            />
                        </TouchableOpacity>
                    </View>
                <View style={{width: 50 + '%',  height: 50,justifyContent: 'center', alignItems: 'flex-end',}}>
                  <Text style={styles.ratesHeading}>Cart</Text>
                </View>
                </View>
                <View style={{flex: 1, flexDirection: 'column', paddingHorizontal: 25, paddingTop: 25,}}>
                    <View style={{width: 100 + '%', paddingBottom: 50,}}>
                        <Text style={styles.subTitle}>TITLE</Text>
                        <Text style={styles.titleText}>{this.state.title}</Text>
                    </View>
                    <View style={{width: 100 + '%', height: 50, marginBottom: 20,}}>
                        <Text style={styles.subTitle}>FROM</Text>
                        <Text style={styles.fromText}>&#8369; {this.state.price}</Text>
                    </View>
                    <View style={{width: 100 + '%', height: 50, marginBottom: 40,}}>
                        <Text style={styles.subTitle}>PLAN</Text>
                        <Text style={styles.fromText}>{this.state.plan}</Text>
                    </View>
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <View style={{width: 100 + '%', height: 20, backgroundColor: 'white',borderTopRightRadius: 18, borderTopLeftRadius: 18,}} />
                </View>
          </ImageBackground>

          <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.welcome}>Description</Text>
            </View>
            <Text style={styles.aboutArea}>{this.state.description}</Text>
            <Text style={styles.hours}>Hours: 9am - 6pm</Text>
            <Text style={styles.hours}>Days: Monday - Sunday</Text>
            <View style={styles.titleAmenities}>
                <Text style={styles.welcome}>Amenities</Text>
            </View>
            {this.renderFacilities()}
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, paddingBottom: 20,}}>
              <TouchableOpacity onPress={this.openBooking.bind(this)} style={styles.book}>
                <Text style={styles.textBook}>BOOK NOW</Text>
              </TouchableOpacity>
            </View>

          </View>
          </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
    hours: {
        color : '#000',
        paddingTop: 15,
        fontWeight: '700',
    },
    aboutArea: {
        color : '#000',
    },
    amenitiesText: {
        color: '#000',
    },
    fromText: {
        fontSize: 18,
        color: '#fff',
        fontWeight: '700',
    },
    subTitle: {
        color: '#e1e1e1',
        fontWeight: '700',
        fontSize: 13,
    },
  titleText: {
    color: '#ffffff',
    fontSize: 25,   
    fontFamily: 'HelveticaNeueLTStd-Bd', 
    fontWeight: '700',
    paddingTop: 5,
    
  },
  plan: { 
    //backgroundColor: 'red', 
    borderTopWidth: 1, 
    borderTopColor: 'black',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    borderLeftColor: 'black',
    borderLeftWidth: 1,
    borderRightColor: 'black',
    borderRightWidth: 1,
    marginBottom: 25,
  },
  textBook: {
    textAlign: 'center',
    fontSize: 24,
    color: 'white',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
  },
  book: {
    marginBottom: 40,
    backgroundColor: 'black',
    width: 100 + '%',
    borderRadius: 20,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    ...Platform.select({
      ios: {             
        paddingTop: 15,
      },
      android: {
        paddingTop: 10,
        paddingBottom: 10,
      },
    }),
  },
  summaryTime: {
    color: '#000000',
    paddingTop: 2,
  },  
  summaryCategory: {
    color: 'gray',
    fontSize: 16,
  },
  summaryDate: {
    color: '#000000',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    fontWeight: '700',
    fontSize: 18,
  },
  listEvents: {
      borderRadius: 16,
      paddingHorizontal: 10,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.10,
          shadowRadius: 16,
          elevation: 24,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    ...Platform.select({
        ios: {             
          paddingTop: 10,
        },
        android: {
         
        },
      }),
    //backgroundColor: 'red'
  },
  titleAmenities: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 20,
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesHeading: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitleList: {
    paddingTop: 20,
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitle: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  back: {
    width: 16,
    height: 16,
  },
  header: {
    //backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    //justifyContent: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    //paddingTop: 30,
    borderTopRightRadius: 18, 
    borderTopLeftRadius: 18,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
