import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, Picker, TextInput, Switch } from 'react-native';
import createDrawerNavigator, { SafeAreaView } from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import ModalSelector from 'react-native-modal-selector';
import moment from 'moment';

class Products extends Component {

  renderProducts = (products) => {
      
      return products.map((item, index) => {

        return (
            <View key={index} style={{padding: 20}}>
                <Button onPress={() => this.props.onPress(item)} title={item.name + " - " + item.price} />
            </View>
        )
      })
  }

  render() {
    return (
        <View style={styles.container}>
            {this.renderProducts(this.props.products)}
        </View>
        
    );
  }
}

export default Products;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
