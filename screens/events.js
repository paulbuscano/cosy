import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity,Picker } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';

export default class Events extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      opening: false,
    }
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {

    var request_1_url = 'https://www.googleapis.com/calendar/v3/calendars/2j77cq5h9a0veetd7c4nh8ltbc%40group.calendar.google.com/events?key=AIzaSyDmDR8qCaaqRK7bZ_pTBCUEk1zl7N35br8';
   
   return fetch(request_1_url).then((response) => response.json()).then((responseJson)  => {

        this.setState({
            isLoading: false,
            dataSource: responseJson.items,
        });
    });
  }

  openEventsSolo(item) {
    if (!this.state.opening) {
      this.props.navigation.push('EventsSolo', {title: item.summary, description: item.description, date: item.start.dateTime, id: item.id})
      this.setState({ opening: true })
      setTimeout(() => {
        this.setState({ opening: false })
      }, 1000)
    }
  }

  //events
  renderEvented = (item, index) => {
    return (
      <View style={{flex: 1, flexDirection: 'column', paddingBottom: 20,}}>
        <FlatList
          //horizontal
          pagingEnabled
          scrollEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          snapToAlignment="center"
          style={{overflow: 'visible'}}
          data={this.state.dataSource}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({item, index}) => this.renderEvents(item, index)}
        />
      </View>
    )
  }


  renderEvents = (item, index) => {
 
    var str = item.start.dateTime;
    var res = str.split('-');

    var dateRight = moment(item.start.dateTime).format('ll');
    var dateLeft = moment(item.start.dateTime).format('ddd');

    if(index === 0) {
      return (
        <View>
          <TouchableOpacity onPress={this.openEventsSolo.bind(this, item)}  style={styles.shadow}>
              <View style={{flex: 1, flexDirection: 'row', marginBottom: 20,}}>
                  <ImageBackground 
                    source={{ uri: 'https://cosy.ph/wp-content/uploads/'+ res[0] +'/'+ res[1] +'/'+ item.id +'.jpg'}}
                    imageStyle={{ borderRadius: 16, opacity: 0.8 }}
                    style={{width: 100 + '%', height: 200, backgroundColor: '#000000', borderTopRightRadius: 16, borderTopLeftRadius: 16,borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16}}
                  >
                  <View style={{ padding: 10, width: 100 + '%', top: 110, position: 'absolute',justifyContent: 'space-evenly',flexDirection: 'column',}}>
                    <Text style={styles.ratesTitle}>{item.summary}</Text>
                    <Text style={styles.ratesPrice}>{dateLeft}, {dateRight}</Text>
                  </View>
                  </ImageBackground>
            </View>
          </TouchableOpacity>
          <View style={styles.title}>
              <Text style={styles.welcome}>Other</Text>
          </View>
      </View>
      )
    }

    return (
      <View>
         <TouchableOpacity onPress={this.openEventsSolo.bind(this, item)}  style={styles.shadow}>
            <View style={styles.listEvents}>
              <Text style={styles.ratesTitleList}>{item.summary}</Text>
              <Text style={styles.ratesPrice}>{dateLeft}, {dateRight}</Text>
            </View>
          </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <View style={{flex: 1}}>
              <View style={styles.header}>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                      style={styles.menu}
                      source={require('../assets/img/menu.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.logo}
                    source={require('../assets/img/logo.png')}
                  />
                </View>
              </View>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            {this.renderEvented()}
          </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listEvents: {
      borderRadius: 16,
      paddingHorizontal: 10,
      justifyContent: 'flex-start',
      flexDirection: 'column',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.10,
          shadowRadius: 16,
          elevation: 24,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingBottom: 20,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //backgroundColor: 'red'
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesTitleList: {
    paddingTop: 30,
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  ratesTitle: {
    fontSize: 20,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  menu: {
    width: 24,
    height: 24,
  },
  header: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 26,
    paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
