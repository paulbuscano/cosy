import React from "react";
import {
    View,
    Text,
    StyleSheet,
    Platform,
    Image,
    TouchableOpacity,
} from "react-native";

import { withNavigation } from 'react-navigation'

import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'

const ShoppingCartIcon = (props) => (
    <View style={{flex: 1,}}>      
            <View style={{position: 'absolute', left: 0, right: 0, bottom: 0, backgroundColor: 'black',borderTopRightRadius: 18, borderTopLeftRadius: 18,}}>
            <TouchableOpacity onPress={() => props.navigation.navigate('Cart')}> 
                <View style={{flex: 1, flexDirection: 'row',}}>
                    <View style={{width: 100 + '%', height: 20, backgroundColor: 'black', borderTopRightRadius: 18, borderTopLeftRadius: 18,}} />
                </View>
                <View style={{flex: 1, flexDirection: 'row',paddingHorizontal: 25, paddingBottom: 15,}}>
                    <View style={{width: 10 + '%', justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Image
                            style={styles.logo}
                            source={require('../assets/img/shopping-cart.png')}
                        />
                    </View>
                    <View style={{width: 30 + '%',  justifyContent: 'center', alignItems: 'flex-start'}}>
                        <Text style={{ fontSize: 18, color: '#fff', fontWeight: '700' }}>My Cart</Text>
                    </View>
                    <View style={{width: 60 + '%',  justifyContent: 'center', alignItems: 'flex-end'}}>
                        <Text style={{ fontSize: 18, color: '#fff', fontWeight: '700' }}>{props.cartItems.length}</Text>
                    </View>
                </View>
            </TouchableOpacity>
            </View>
    </View>
)

const mapStateToProps = (state) => {
    return {
        cartItems: state
    }
}

export default connect(mapStateToProps)(withNavigation(ShoppingCartIcon));

const styles = StyleSheet.create({
    logo: {
        width: 24,
        height: 24,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconContainer: {
        paddingLeft: 20, paddingTop: 10, marginRight: 5,
        paddingHorizontal: 25,
        backgroundColor: 'black',
    }
});