import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, TextInput, StatusBar } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';

export default class Contact extends Component {
  render() {
    return (
      
      <ImageBackground 
        source={require('../assets/img/contact-bg.jpeg')}
        imageStyle={{ opacity: 0.9 }}
        style={{resizeMode: 'cover',flex: 1, backgroundColor: '#000000',}}
      >
      <View style={{flex: 1, paddingHorizontal: 25,}}>
      <StatusBar barStyle="light-content" />
              <View style={styles.header}>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                      style={styles.menu}
                      source={require('../assets/img/menu-white.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.logo}
                    source={require('../assets/img/contact-logo-white-final.png')}
                  />
                </View>
              </View>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column'}}>
              <Text style={styles.contactLabel}>Visit us: 92 Gil Puyat Avenue</Text>
              <Text style={styles.contactLabelSecond}>Barangay San Isidro</Text>
              <Text style={styles.contactLabelThird}>Makati City 1234</Text>
              <Text style={styles.contactLabel}>Call us: (02) 952 2254</Text>
              <Text style={styles.contactLabelContact}>(02) 420 6441</Text>
              <Text style={styles.contactLabelContactThird}>(+63) 917 334 1010</Text>
              <Text style={styles.contactLabelLast}>Email us: hello@cosy.ph</Text>
            </View>
          </View>
          <View style={styles.listEvents}>
            <View style={styles.title}>
              <Text style={styles.welcome}>Contact us</Text>
            </View>
              <TextInput style = {styles.input}
                  underlineColorAndroid = "transparent"
                  placeholder = " Name"
                  placeholderTextColor = "#e3e3e3"
                  autoCapitalize = "none"
                  onChangeText = {this.handleEmail}
              />
              <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = " Email Address"
                placeholderTextColor = "#e3e3e3"
                autoCapitalize = "none"
                onChangeText = {this.handleEmail}
              />
              <TextInput style = {styles.inputLast}
                underlineColorAndroid = "transparent"
                placeholder = " Contact #"
                placeholderTextColor = "#e3e3e3"
                autoCapitalize = "none"
                onChangeText = {this.handleEmail}
              />
              
              <TouchableOpacity>
                <View style={styles.submitContainer}>
                <Text style={styles.submit}>SUBMIT</Text>
                </View>
              </TouchableOpacity>
              
          </View>

          </ScrollView>
      </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    submitContainer: {
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center',
      width: 40 + '%',
      backgroundColor:'#000000',
      borderRadius:20,
      marginHorizontal: 10,
      paddingBottom: 7,
      paddingTop: 7,
      ...Platform.select({
        ios: {             
          marginTop: 10,
          paddingTop: 14,
          width: 45 + '%',
        },
        android: {
          marginTop: 10,
      
        },
      }),
    },
    submit: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
    },
      input: {
        margin: 10,
        height: 40,
        borderColor: '#000',
        borderWidth: 1,
    },
    inputLast: {
      margin: 10,
      height: 40,
      borderColor: '#000',
      borderWidth: 1,
    },
    contactLabelContactThird: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      paddingBottom: 15,
      ...Platform.select({
        ios: {             
          paddingLeft: 69,
        },
        android: {
          paddingLeft: 72,
        },
      }),
    },
    contactLabelContact: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      paddingBottom: 5,
      ...Platform.select({
        ios: {             
          paddingLeft: 69,
        },
        android: {
          paddingLeft: 72,
        },
      }),
    },
    contactLabelThird: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      ...Platform.select({
        ios: {             
          paddingLeft: 72,
        },
        android: {
          paddingLeft: 79,
        },
      }),
      paddingBottom: 15,
    },
    contactLabelSecond: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      ...Platform.select({
        ios: {             
          paddingLeft: 72,
        },
        android: {
          paddingLeft: 79,
        },
    }),
      paddingBottom: 5,
    },
    contactLabelLast: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      marginBottom: 30,
    },
    contactLabel: {
      color: '#fff',
      fontWeight: '700',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      fontSize: 18,
      paddingBottom: 5,
    },
    reviewText: {
      color: '#555555',
    },
    reviewName: {
      fontWeight: '700',
      color: '#000000',
      fontFamily: 'HelveticaNeueLTStd-Bd',
      paddingBottom: 3,
    },
    aboutArea: {
        color: '#000',
        marginBottom: 20,
    },
    listEvents: {
      paddingHorizontal: 10,
      justifyContent: 'flex-start',
      borderRadius: 16,
      flexDirection: 'column',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 350, 
      backgroundColor: 'white',
      ...Platform.select({
        ios: {             
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 6,
          },
          shadowOpacity: 0.37,
          shadowRadius: 7.49,
          elevation: 12,
        },
        android: {
          shadowOpacity: 0.75,
          shadowRadius: 5,
          shadowColor: 'black',
          shadowOffset: { height: 0, width: 0 },
          elevation: 10,
          borderRadius: 16,
        },
    }),
  },
  welcome: {
    fontSize: 25,
    textAlign: 'left',
    fontFamily: 'HelveticaNeueLTStd-Bd',
    color: '#000000',
    paddingTop: 30,
    marginBottom: 10,
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 10,
    //backgroundColor: 'red'
  },
  ratesPriceList: {
    fontSize: 14,
    color: '#c4c4c4',
  },
  ratesPrice: {
    fontSize: 15,
    color: '#e3e3e3',
  },
  ratesTitleList: {
    fontSize: 18,
    color: '#000000',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  aboutTitle: {
    fontSize: 25,
    color: '#ffffff',
    fontWeight: '700',
    fontFamily: 'HelveticaNeueLTStd-Bd',
  },
  logo: {
    width: 76,
    height: 36,
  },
  menu: {
    width: 24,
    height: 24,
  },
  header: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 26,
    //paddingHorizontal: 25, 
    //paddingBottom: 26,
  },
  container: {
    backgroundColor: 'transparent',
    //paddingHorizontal: 25,
    paddingTop: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
