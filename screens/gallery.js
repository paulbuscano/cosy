import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import createDrawerNavigator from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import Grid from 'react-native-grid-component';

export default class Gallery extends Component {

    _renderItem = (data, i) => (
        <View style={styles.item} key={i}>
            <ImageBackground 
            source={{ uri: data }} 
            imageStyle={{ borderRadius: 16, opacity: 0.8 }}
            style={{width: 100 + '%', height: 150, backgroundColor: '#000000', borderTopRightRadius: 16, borderTopLeftRadius: 16,borderRadius: 16, borderTopLeftRadius: 16, borderTopRightRadius: 16}}
        >
        </ImageBackground>
      </View>
    );

    

    _renderPlaceholder = i => <View style={styles.item} key={i} />;

  render() {
    
    return (
       
      <View style={{flex: 1}}>
              <View style={styles.header}>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center'}}>
                  <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Image
                      style={styles.menu}
                      source={require('../assets/img/menu.png')}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{width: 50 + '%', height: 50, justifyContent: 'center', alignItems: 'flex-end'}}>
                  <Image
                    style={styles.logo}
                    source={require('../assets/img/logo.png')}
                  />
                </View>
              </View>
          <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
          <View style={styles.title}>
              <Text style={styles.welcome}>@cosyph</Text>
          </View>
            <Grid
                style={styles.list}
                renderItem={this._renderItem}
                renderPlaceholder={this._renderPlaceholder}
                data={['https://cosy.ph/wp-content/uploads/2018/12/IMG_8380.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Bean-1.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Cup-2.jpg', 

                'https://cosy.ph/wp-content/uploads/2018/12/Desk-4.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Group-1.jpeg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Group-3.jpg',

                'https://cosy.ph/wp-content/uploads/2018/12/Signage-2.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-1.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-2.jpg', 

                'https://cosy.ph/wp-content/uploads/2018/12/Hotdesk-3.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_9035-copy.jpg', 
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8526.jpg',

                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8528.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8849-copy.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/Private-Office3.jpg',

                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8842-copy.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8823-copy.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8491.jpg', 

                'https://cosy.ph/wp-content/uploads/2018/12/IMG_9046-copy.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8791-1.jpg',
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8891-copy.jpg',
                
                'https://cosy.ph/wp-content/uploads/2018/12/IMG_8867-copy.jpg', ]}
                numColumns={3}
            />
          </View>
          </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    item: {
        flex: 1,
        height: 150,
        margin: 5,
        borderRadius: 16,
    },
    list: {
        flex: 1
    },
    listEvents: {
      paddingHorizontal: 10,
      justifyContent: 'flex-start',
      flexDirection: 'column',
      marginBottom: 18, 
      width: 100 + '%', 
      height: 100, 
      backgroundColor: 'white',
      ...Platform.select({
            ios: {             
            shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.37,
            shadowRadius: 7.49,
            elevation: 12,
            },
            android: {
            shadowOpacity: 0.75,
            shadowRadius: 5,
            shadowColor: 'black',
            shadowOffset: { height: 0, width: 0 },
            elevation: 10,
            borderRadius: 16,
            },
        }),
    },
    welcome: {
        fontSize: 25,
        textAlign: 'left',
        fontFamily: 'HelveticaNeueLTStd-Bd',
        color: '#000000',
        paddingBottom: 20,
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        //backgroundColor: 'red'
    },
    ratesPriceList: {
        fontSize: 14,
        color: '#c4c4c4',
    },
    ratesPrice: {
        fontSize: 15,
        color: '#e3e3e3',
    },
    ratesTitleList: {
        paddingTop: 20,
        fontSize: 18,
        color: '#000000',
        fontWeight: '700',
        fontFamily: 'HelveticaNeueLTStd-Bd',
    },
    ratesTitle: {
        fontSize: 20,
        color: '#ffffff',
        fontWeight: '700',
        fontFamily: 'HelveticaNeueLTStd-Bd',
    },
    logo: {
        width: 76,
        height: 36,
    },
    menu: {
        width: 24,
        height: 24,
    },
    header: {
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 26,
        paddingHorizontal: 25, 
        //paddingBottom: 26,
    },
    container: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 25,
        paddingTop: 30,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
