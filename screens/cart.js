import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image, FlatList, ScrollView, ImageBackground, TouchableOpacity, Picker, TextInput, Switch } from 'react-native';
import createDrawerNavigator, { SafeAreaView } from 'react-navigation';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import ModalSelector from 'react-native-modal-selector';
import moment from 'moment';

import Products from '../components/Products';
import { connect } from 'react-redux';

class Cart extends Component {



    renderRecommended = (item, index) => {
        return (
          <View style={{ paddingBottom: 20 }}>
            <FlatList
              pagingEnabled
              scrollEnabled
              showsHorizontalScrollIndicator={false}
              scrollEventThrottle={16}
              snapToAlignment="center"
              style={{overflow: 'visible'}}
              data={this.props.cartItems}
              keyExtractor={(item, index) => `${item.id}`}
              renderItem={({item, index}) => this.renderRecommendation(item, index)}
            />
          </View>
        )
      }

      test() {
        let sum = this.props.cartItems.reduce(function(prev, current) {
            return prev + +current.price
        }, 0);
       return sum;
      }

      renderRecommendation = (item, index) => {


  
        return (
            <View style={{flex: 1, flexDirection: 'row', marginBottom: 40,}}>
                <View style={{width: 25 + '%', height: 70,  flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingRight: 10,}}>
                    <Image
                        style={styles.orderImage}
                        source={{uri: item.image}}
                    />
                </View>
                <View style={{width: 25 + '%', height: 70,  flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.bold}>{item.date}</Text>
                    <Text style={styles.bold}>{item.time}</Text>
                </View>
                <View style={{width: 30 + '%', height: 70,  flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingLeft: 10,}}>
                    <Text style={styles.bold}>{item.title}</Text>
                    <Text style={styles.bold}>({item.plan})</Text>
                </View>
                <View style={{width: 20 + '%', height: 70,  flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                    <Text>&#8369; {item.price}</Text>
                </View>
            </View>
        )
    }



    render() {
  
        return (
            <View style={{flex: 1}}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.header}>
                    <View style={{width: 40 + '%', height: 50, justifyContent: 'center', }}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}> 
                        <Image
                        style={styles.back}
                        source={require('../assets/img/left-arrow-final.png')}
                        />
                    </TouchableOpacity>
                    </View>
                    <View style={{width: 60 + '%', height: 50,justifyContent: 'center', alignItems: 'flex-end',}}>
                    <Text style={styles.ratesHeading}>My cart</Text>
                    </View>
                </View>
                <View style={styles.container}>
                    <View style={styles.title}>
                        <Text style={styles.welcome}>My Order</Text>
                    </View>
                    {this.renderRecommended()}
                    {this.props.cartItems.length > 0 ?
                    <View style={{flex: 1, flexDirection: 'row', marginBottom: 20, }}>
                        <View style={{width: 50 + '%', height: 50,  flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',}}>
                            <Text style={styles.welcomeTotal}>Total: </Text>
                        </View>
                        <View style={{width: 50 + '%', height: 50, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center',}}>
                            <Text style={styles.welcomeTotalPrice}>&#8369; {this.test()}</Text>
                        </View>
                    </View> 

                    :<View style={{flex: 1, flexDirection: 'row',}}> 
                        <View style={{width: 100 + '%',}}>
                            <Text style={{textAlign: 'center', fontSize: 20, paddingBottom: 30,}}>No items in your cart</Text>
                        </View>
                    </View>
                    }
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15,}}>
                        <TouchableOpacity style={styles.checkOut}>
                            <Text style={styles.textBook}>CHECK OUT</Text>
                        </TouchableOpacity>
                    </View>          
                    {/* {this.props.cartItems.length > 0 ?
                    <Products
                        onPress={this.props.removeItem}
                        products={this.props.cartItems} />
                    : <Text>No items in your cart</Text>
                    } */}
                </View>
            </ScrollView>
            </View>      
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        removeItem: (product) => dispatch({ type: 'REMOVE_FROM_CART' , payload: product})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

const styles = StyleSheet.create({
    textBook: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white',
        fontFamily: 'HelveticaNeueLTStd-Bd',
        fontWeight: '700',
    },
    checkOut: {
        marginBottom: 40,
        backgroundColor: 'black',
        width: 100 + '%',
        borderRadius: 20,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        ...Platform.select({
          ios: {             
            paddingTop: 15,
          },
          android: {
            paddingTop: 10,
            paddingBottom: 10,
          },
        }),
    },
    bold: {
        fontWeight: '700',
        color: '#000',
    },
    orderImage: {
        width: 100 + '%',
        height: 70,
    },
    welcomeTotal: {
        fontSize: 25,
        textAlign: 'left',
        fontFamily: 'HelveticaNeueLTStd-Bd',
        color: 'gray',
    },
    welcomeTotalPrice: {
        fontSize: 30,
        textAlign: 'left',
        fontFamily: 'HelveticaNeueLTStd-Bd',
        color: '#000000',
    },
    welcome: {
        fontSize: 25,
        textAlign: 'left',
        fontFamily: 'HelveticaNeueLTStd-Bd',
        color: '#000000',
        paddingBottom: 20,
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginBottom: 20,
    },
    container: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 25,
        paddingTop: 30,
    },
    header: {
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        alignItems: 'center',
        //justifyContent: 'center',
        paddingTop: 26,
        paddingHorizontal: 25, 
        //paddingBottom: 26,
    },
    back: {
        width: 16,
        height: 16,
    },
    ratesHeading: {
        fontSize: 20,
        color: '#000000',
        fontWeight: '700',
        fontFamily: 'HelveticaNeueLTStd-Bd',
      },
});